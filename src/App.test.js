import React from 'react'
import { mount, configure }  from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import App from './App'

configure({ adapter: new Adapter() })

describe('App', () => {
  it('store interaction', () => {
    const prevent = jest.fn()
    const reducer = jest.fn().mockReturnValue({
      finance: [{ desc: 'todo 1', cant: 150 }]
    })
    const store = createStore(reducer)

    const wrapper = mount(
      <Provider store={store}>
        <App />
      </Provider>
    )

    wrapper
      .find('input')
      .at(0)
      .simulate('change', { target: { value: 'todo 2' } })
    wrapper
      .find('input')
      .at(1)
      .simulate('change', { target: { value: '200' } })
    wrapper.find('form').simulate('submit', { preventDefault: prevent })
    wrapper
      .find('button')
      .at(1)
      .simulate('click')
    
    const [und, ...rest] = reducer.mock.calls

    expect(rest).toEqual([
      [
        { finance: [{ desc: 'todo 1', cant: 150 }] },
        { type: 'ADD', payload: { cant: 200, desc: 'todo 2' }}
      ],
      [
        { finance: [{ desc: 'todo 1', cant: 150 }] },
        { type: 'DELETE', index: 0 }
      ]
    ])

    expect(wrapper.text().includes('todo 1')).toEqual(true)
  })
})