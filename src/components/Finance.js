import React from "react";

export default function Finance({ finance, deleteFinance }) {
  return (
    <div className="column is-half">
      <table className="table is-striped is-fullwidth">
        <thead>
          <tr>
            <th>Description</th>
            <th>Among</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {finance.map((x, i) => (
            <tr key={i}>
              <td>{x.desc}</td>
              <td>{x.cant}</td>
              <td>
                <button
                  className="button is-warning"
                  onClick={() => deleteFinance(i)}
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
