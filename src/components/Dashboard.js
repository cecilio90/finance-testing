import React from "react";

export default function Dashboard({ value }) {
  return (
    <div className="column is-half">
      <div className="box">
        <p>Total</p>
        <strong>{value}</strong>
      </div>
    </div>
  );
}
