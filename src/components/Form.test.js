import React from 'react'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Form from './Form'

configure({ adapter: new Adapter })

describe('Form to send personal finance', () => {
  it('Add finance', () => {
    const addFinance = jest.fn()
    const prevent = jest.fn()

    const wrapper = shallow(<Form addFinance={addFinance}/>)

    wrapper
      .find('input')
      .at(0)
      .simulate('change', { target: { value: 'description' } })
    wrapper
      .find('input')
      .at(1)
      .simulate('change', { target: { value: '150' } })
    wrapper
      .find('form')
      .simulate('submit', { preventDefault: prevent })
    
    expect(addFinance.mock.calls).toEqual([[{ desc: 'description', cant: 150 }]])
    expect(prevent.mock.calls).toEqual([[]])
  })
})
