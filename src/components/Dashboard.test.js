import React from 'react'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Dashboard from './Dashboard'

configure({ adapter: new Adapter() })

describe('', () => {
  it('', () => {
    const value = 1000
    const wrapper = shallow(<Dashboard value={value}/>)
    const response = wrapper.text().includes('1000')
    
    expect(response).toEqual(true)
  })
})
