import React from 'react'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Finance from './Finance'

configure({ adapter: new Adapter() })

describe('Finance', () => {
  it('call deleteFinance', () => {
    const finance = [
      { desc: 'finance 1', cant: 100 },
      { desc: 'finance 2', cant: 80 },
    ]
    const deleteFinance = jest.fn()

    const wrapper = shallow(<Finance
      finance={finance}
      deleteFinance={deleteFinance}
    />)

    wrapper
      .find('button')
      .at(0)
      .simulate('click')
    
    expect(deleteFinance.mock.calls).toEqual([[0]])

    const response1 = wrapper.text().includes('finance 1')
    expect(response1).toEqual(true)
    const response2 = wrapper.text().includes('finance 2')
    expect(response2).toEqual(true)
  })
})