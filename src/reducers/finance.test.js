import reducer, { addFinance, deleteFinance } from './finance'

describe('Finance Duck', () => {
  describe('actions creator', () => {
    it('add finance', () => {
      const response = addFinance(1)
      expect(response).toEqual({
        type: 'ADD',
        payload: 1
      })
    })

    it('delete finance', () => {
      const response = deleteFinance(1)
      expect(response).toEqual({
        type: 'DELETE',
        index: 1
      })
    })
  })

  describe('Reducers', () => {
    it('ADD', () => {
      const response = reducer([1], {
        type: 'ADD',
        payload: 2
      })
      expect(response).toEqual([1, 2])
    })

    it('DELETE', () => {
      const response = reducer([1, 2], {
        type: 'DELETE',
        payload: 2
      })
      expect(response).toEqual([2])
    })

    it('DEFAULT', () => {
      const response = reducer([1], {
        type: 'NOTHING',
      })
      expect(response).toEqual([1])
    })
  })
})