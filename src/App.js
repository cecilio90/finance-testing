import React from "react";
import { connect } from "react-redux";
import { addFinance, deleteFinance } from "./reducers/finance";
import Form from "./components/Form";
import Dashboard from "./components/Dashboard";
import Finance from "./components/Finance";
import "./App.css";

function Titulo() {
  return <h2 className="title">Finanzly</h2>;
}

function App({ finance, addFinance, deleteFinance }) {
  const total = finance.reduce((acc, el) => acc + el.cant, 0);
  return (
    <div className="section">
      <div className="container">
        <Titulo />
        <Form addFinance={addFinance} />
        <Dashboard value={total} />
        <Finance finance={finance} deleteFinance={deleteFinance} />
      </div>
    </div>
  );
}

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = dispatch => ({
  addFinance: finanza => dispatch(addFinance(finanza)),
  deleteFinance: index => dispatch(deleteFinance(index))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
